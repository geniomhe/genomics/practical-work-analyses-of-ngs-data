#!/usr/bin/env -S awk -f
BEGIN {
	SNP = 0
	indel = 0
}

/^[^#]/ {
	chr = $1
	variants[chr]++
	if (length($4) == 1 && length($5) == 1) {
		SNP++
		SNPs[chr]++
	} else {
		indel++
		indels[chr]++
	}
}

END {
	print "per chr:"
	print "chr", "count"
	for (chr in variants) {
		print chr, variants[chr]
	}
	printf "\n"
	print "per type:"
	print "type", "count"
	print "SNP", SNP
	print "indel", indel
	print "\n"
	print "per chr per type:"
	print "chr", "type", "count"
	type = "SNP"
	for (chr in SNPs) {
		print chr, "SNP", SNPs[chr]
	}
	type = "indel"
	for (chr in indels) {
		print chr, "indel", indels[chr]
	}
}
