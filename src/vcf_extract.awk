#!/usr/bin/env -S awk -f
BEGIN {
	OFS = "\t"
	FS = "\t"
	# Add header line
	print "Chrom", "Pos", "REF", "ALT", "TYPE", "FILTER", "Cons", "GENE", "AF_EUR", "CADD", "Child", "DP_Child", "Mother", "DP_Mother", "Father", "DP_Father"
}

/^[^#]/ {
	chr = $1
	pos = $2
	ref = $4
	alt = $5
	filter = $7
	info = $8
	# Deduce the type of variant from the length of the reference or alternative form
	if (length(ref) == 1 && length(alt) == 1) {
		type = "SNP"
	} else {
		type = "indel"
	}
	# Extract some sub columns from the INFO column
	split(info, info_array, "|")
	cons = info_array[2]
	GENE = info_array[5]
	AF_EUR = info_array[42]
	CADD = info_array[73]
	# Extract GT and DP from each member of the trio
	Child = $10
	Mother = $11
	Father = $12
	extract_gt_dp(Child, Child_res)
	extract_gt_dp(Mother, Mother_res)
	extract_gt_dp(Father, Father_res)
	print chr, pos, ref, alt, type, filter, cons, GENE, AF_EUR, CADD, Child_res["GT"], Child_res["DP"], Mother_res["GT"], Mother_res["DP"], Father_res["GT"], Father_res["DP"]
}


function extract_gt_dp(member, res)
{
	split(member, member_info, ":")
	res["GT"] = member_info[1]
	res["DP"] = member_info[4]
}
