#!/usr/bin/env -S awk -f
# Filter the row corresponding to certain de novo variants
BEGIN {
	FS = "\t"
}

{
	Child = $11
	Mother = $13
	Father = $15
	if (is_denovo(Child, Mother, Father)) {
		print $0
	}
}


function is_denovo(Child, Mother, Father)
{
	if (Child == "./.") {
		return 0
	}
	split(Child, Child_GT, "/")
	split(Mother, Mother_GT, "/")
	split(Father, Father_GT, "/")
	for (i = 1; i <= 2; i++) {
		if (Mother_GT[i] == "." || Father_GT[i] == ".") {
			return 0
		}
	}
	for (i in Child_GT) {
		Child_allele = Child_GT[i]
		if (Child_allele == Mother_GT[1] || Child_allele == Mother_GT[2] || Child_allele == Father_GT[1] || Child_allele == Father_GT[2]) {
			return 0
		}
	}
	return 1
}
