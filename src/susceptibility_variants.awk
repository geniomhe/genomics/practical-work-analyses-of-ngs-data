#!/usr/bin/env -S awk -f
NR == 1 {
   print $0
}
NR > 1 {
    Child=$11
    Mother=$13
    Father=$15

    split(Child, Child_GT, "/")
    split(Mother, Mother_GT, "/")
    split(Father, Father_GT, "/")

    # Father and Child must be homozygous with same genotype
    if (Child != "./." && Child == Father && Child_GT[1] == Child_GT[2]) {
        # Mother must have at least one variant that is different
        for (i=1; i <= 2; i++) {
            if (Mother_GT[i] != Child_GT[1] && Mother_GT[i] != ".") {
                print $0
            }
        }
    }
}
